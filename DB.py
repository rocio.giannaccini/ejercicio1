import  aiomysql

class DB:
    
    def __init__(self):
            self.connection = None
        
    async def init_connection(self):
        if self.connection is None:
            pool = await aiomysql.create_pool(
                    host='localhost',
                    user='root',
                    password='test',
                    db='ejercicio_1' 
                )
            self.connection = await pool.acquire()

    async def get_connection(self):
        if self.connection is None:
            await self.init_connection()
        return self.connection
    
    async def release_connection(self):
        if self.connection is not None:
            self.connection.close()
            self.connection = None
